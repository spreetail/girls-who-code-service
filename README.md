# girls-who-code-service
service for expensive and cheap items.

## Uasge

Install server with:

```
$ npm i
```

Start server with:

```
$ npm start
```

Then brownse to http://localhost:8080/swagger to see the service and docs.


Further reading:

Learn about [swagger](https://swagger.io/) for designing and developing developing APIs.

Use [express](https://expressjs.com/) and [swaggerize-express](https://github.com/krakenjs/swaggerize-express) to build your APIs.

Node goodness at [npmjs](http://npmjs.org).



