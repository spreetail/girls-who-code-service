const convict = require('convict');
const path = require('path');

const schema = {
  serverIp: {
    doc: 'The IP address to bind.',
    default: '127.0.0.1',
    env: 'SERVER_IP',
  },
  serverPort: {
    doc: 'The port to bind.',
    default: 8080,
    env: 'SERVER_PORT',
  },
  swaggerPath: {
    doc: 'Path to swagger-file.',
    default: './swagger.json',
    env: 'SWAGGER_PATH',
  },
};

const config = convict(schema);

config.loadFile(path.join(__dirname, 'envs', `${process.NODE_ENV || 'local-dev'}.json`));

module.exports = config;
