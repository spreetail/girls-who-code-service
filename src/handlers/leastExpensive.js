
const httpStatusCodes = require('http-status-codes');
const axios = require('axios');

const { shapeData } = require('../lib/utils.js');

/**
 * Operations for /least-expensive
 */
module.exports = {
  /**
     * summary: get the top x (defaults to 10) most least items
     * parameters: count
     * responses: 200, default
     */
  get: async function leastExpensive(req, res) {
    const count = parseInt(req.query.count, 10 || '10');
    const url = `https://uat.spreetest.com/api/search/products?page=1&size=${count}&sortBy=price&orderBy=asc&priceMin=0`;
    const { status, data } = await axios.get(url);
    if (status !== 200) {
      throw new Error(`Down stream service sent status = ${status}, ${url}`);
    }
    const output = shapeData(data);
    res.status(httpStatusCodes.OK).send(output);
  },
};
