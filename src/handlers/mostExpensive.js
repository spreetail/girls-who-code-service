const httpStatusCodes = require('http-status-codes');
const axios = require('axios');

const { shapeData } = require('../lib/utils.js');

/**
 * Operations for /most-expensive
 */
module.exports = {
  /**
     * summary: get the top x (defaults to 10) most expense items
     * parameters: count
     * responses: 200, default
     */
  get: async function mostExpensive(req, res) {
    const count = parseInt(req.query.count, 10 || '10');
    const url = `https://uat.spreetest.com/api/search/products?page=1&size=${count}&sortBy=price&priceMin=0`;
    const { status, data } = await axios.get(url);
    if (status !== 200) {
      throw new Error(`Down stream service sent status = ${status}, ${url}`);
    }
    const output = shapeData(data);
    res.status(httpStatusCodes.OK).send(output);
  },
};
