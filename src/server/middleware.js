/* eslint-disable no-unused-vars */
const cors = require('cors');
const pino = require('express-pino-logger');
const bodyParser = require('body-parser');
const express = require('express');

// set up the middleware for this server.
function middleware(app, config) {
  app.use(pino());
  app.use(cors());
  app.options('*', cors());
  app.use(bodyParser.json());
  app.use(express.json());
  app.use(bodyParser.urlencoded({ extended: true }));
}

module.exports = middleware;
