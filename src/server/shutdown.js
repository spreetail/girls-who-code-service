// handle server shutdown

function shutdown(type, server, logger) {
  return async () => {
    logger.info('Shutting down server:', type);
    server.close();
  };
}

module.exports = shutdown;
