/* eslint-disable import/no-dynamic-require */
const swaggerize = require('swaggerize-express');
const swaggerUi = require('swagger-ui-express');
const path = require('path');

function swagger(app, config) {
  // set up swagger and api
  const swaggerPath = config.get('swaggerPath');
  const swaggerDoc = require(swaggerPath);
  const api = path.join(__dirname, swaggerPath);
  const handlers = '../handlers';
  app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDoc));
  app.use('/swagger.json', (req, res) => res.sendFile(api));
  app.use(swaggerize({ api, handlers }));
}

module.exports = swagger;
