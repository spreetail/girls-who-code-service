const middelware = require('./middleware');
const resources = require('./resources');
const shutdown = require('./shutdown');
const swagger = require('./swagger');

module.exports = {
  middelware, resources, shutdown, swagger,
};
