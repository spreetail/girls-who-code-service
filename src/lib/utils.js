function shapeData(data) {
  return data.products.map(product => ({
    title: product.title,
    salePrice: product.buyboxPrice,
    images: product.imageUrls.splice(0, 2),
    category: product.categoryAncestors,
    description: product.description,
    terms: product.searchTerms,
    manufacturer: product.manufacturer,
    catalogSlug: product.categorySlug,
    msrp: product.msrp,
    productUrl: `http://www.spreetail.com/${product.slug}/${product.spin}/p`,
  }));
}

module.exports = { shapeData };
