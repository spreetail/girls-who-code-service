/* eslint-disable no-console, spaced-comment, no-multi-assign */

/****************************************************
 * AVOID EDITING THI FILE THIS FILE UNLESS NECESSARY
 * The files in the ./server folder allow for setting
 * up middleware, resoureces, etc.  Please try to use
 * them.  Thanks.
 ****************************************************/

const http = require('http');
const express = require('express');
const pino = require('pino');
const config = require('../config');
const {
  middelware, swagger, resources, shutdown,
} = require('./server');

async function start() {
  // initialize server
  const app = express();

  app.locals.config = config;
  const appLogger = app.locals.logger = pino();

  middelware(app, config);
  swagger(app, config);
  resources(app, config);

  // create server and start listening.
  const port = config.get('serverPort');
  const host = config.get('serverIp');
  const server = http.createServer(app);
  server.listen(port, host, () => {
    app.swagger.api.host = `${host}:${port}`;
    appLogger.info('App running on %s:%d', host, port);
  });

  // handle shutdown
  process.on('SIGINT', shutdown('SIGINT', server, appLogger));
  process.on('SIGTERM', shutdown('SIGTERM', server, appLogger));

  // return server for testing.
  return server;
}

if (require.main === module) {
  // this has been run from the command line, just start the server.
  start().catch(console.err);
} else {
  // this is acting like a module, export the start function
  module.exports = start;
}
